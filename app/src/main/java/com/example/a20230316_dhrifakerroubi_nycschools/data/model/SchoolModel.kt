package com.example.a20230316_dhrifakerroubi_nycschools.data.model

data class SchoolModel(
    val dbn: String,
    val school_name: String,
    val website: String
)