package com.example.a20230316_dhrifakerroubi_nycschools.ui.schools

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.a20230316_dhrifakerroubi_nycschools.data.model.SchoolModel
import com.example.a20230316_dhrifakerroubi_nycschools.databinding.SchoolItemBinding

private const val TAG = "SchoolsAdapter"
class SchoolsAdapter (
    private val schools: List<SchoolModel>,
    private val showDetails: (String) -> Unit
): RecyclerView.Adapter<SchoolsAdapter.SchoolViewHolder>() {
    inner class SchoolViewHolder(private val binding: SchoolItemBinding):
        RecyclerView.ViewHolder(binding.root) {
        fun bind(school: SchoolModel) {
            Log.d(TAG, "bind: ")
            binding.apply {
                schoolName.text = school.school_name
                schoolWebsite.text = school.website
                scoreButton.setOnClickListener {
                    showDetails(school.dbn)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolViewHolder {
        return SchoolViewHolder(
            SchoolItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: SchoolViewHolder, position: Int) {
        holder.bind(schools[position])
    }

    override fun getItemCount(): Int {
        return schools.size
    }
}