package com.example.a20230316_dhrifakerroubi_nycschools.data

import com.example.a20230316_dhrifakerroubi_nycschools.data.model.SchoolModel
import com.example.a20230316_dhrifakerroubi_nycschools.data.model.ScoreModel
import com.example.a20230316_dhrifakerroubi_nycschools.data.remote.SchoolApi
import retrofit2.Response
import javax.inject.Inject

interface Repository {

    suspend fun getSchools(): Response<List<SchoolModel>>

    suspend fun getScore(dbn: String): Response<List<ScoreModel>>

}


class RepositoryImpl @Inject constructor(private val schoolsApi: SchoolApi) : Repository {

    override suspend fun getSchools(): Response<List<SchoolModel>> {
        return schoolsApi.getSchools()
    }

    override suspend fun getScore(dbn: String): Response<List<ScoreModel>> {
      return  schoolsApi.getCurrentScore(dbn)
    }

}