package com.example.a20230316_dhrifakerroubi_nycschools.data.remote

import com.example.a20230316_dhrifakerroubi_nycschools.data.model.SchoolModel
import com.example.a20230316_dhrifakerroubi_nycschools.data.model.ScoreModel
import com.google.android.material.color.utilities.Score
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface SchoolApi {
    @GET(SCHOOL_ENDPOINT)
    suspend fun getSchools(): Response<List<SchoolModel>>

    @GET(SCORE_ENDPOINT)
    suspend fun getCurrentScore(
        @Query("dbn") dbn: String
    ): Response<List<ScoreModel>>


    companion object {
        const val BASE_URL = "https://data.cityofnewyork.us/resource/"
        const val SCHOOL_ENDPOINT = "s3k6-pzi2.json"
        const val SCORE_ENDPOINT = "f9bf-2cp4.json"

//        var instance: Retrofit? = null
//        fun getRetrofit(): Retrofit {
//            if (instance == null) {
//                instance = Retrofit.Builder()
//                    .baseUrl(BASE_URL)
//                    .addConverterFactory(GsonConverterFactory.create())
//                    .build()
//            }
//            return instance!!
//        }
    }
}